package cn.iune;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(TestApplication.class, args);
		System.out.println("ctx.getApplicationName()-->>"+ctx.getApplicationName());
		String displayName = ctx.getDisplayName();
		System.out.println("ctx.getDisplayName()-->>"+displayName);
		System.out.println("ctx.getId()-->>"+ctx.getId());
		System.out.println("ctx.getBeanDefinitionCount()-->>"+ctx.getBeanDefinitionCount());

//		String[] beanNames = ctx.getBeanDefinitionNames();
//		System.out.println("==============================================");
//		for(int i=0;i<beanNames.length;i++) {
//			System.out.println(beanNames[i]);
//		}
//		System.out.println("==============================================");
	}
	
}
